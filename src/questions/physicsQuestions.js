import React, { Component } from 'react';
import { AppContext } from '../context';
import Modal from 'react-modal';
import ShowResult from '../resultPage/showResult';
import LoginPage from '../loginPageDetails/loginPage'

const customStyles = {
    content: {
      top: '50%',
      left: '50%',
      right: 'auto',
      bottom: 'auto',
      marginRight: '-50%',
      transform: 'translate(-50%, -50%)',
    },
  };

export default class PhysicsQuestions extends Component {
    static contextType = AppContext;
    constructor(props) {
      super(props);
      this.state = {
          index: 0, 
          modalIsOpen: false,
          afterOpenModal: false, 
          closeModal:false, 
          showResult: false,
          showLoginPage: false,
          time: {}, 
          seconds: 300
        }
        this.timer = 0;
        this.startTimer = this.startTimer.bind(this);
        this.countDown = this.countDown.bind(this);
    }

    secondsToTime(secs){
        let hours = Math.floor(secs / (60 * 60));
    
        let divisor_for_minutes = secs % (60 * 60);
        let minutes = Math.floor((divisor_for_minutes / 60),2);
    
        let divisor_for_seconds = divisor_for_minutes % 60;
        let seconds = Math.ceil(divisor_for_seconds);
    
        let obj = {
          "h": hours,
          "m": minutes,
          "s": seconds
        };
        return obj;
      }

      componentDidMount() {
        let timeLeftVar = this.secondsToTime(this.state.seconds);
        this.setState({ time: timeLeftVar });
        this.startTimer();
      }

      componentDidUpdate() {
        if(this.state.time.m == 0 && this.state.time.s == 0) {
            this.state.modalIsOpen =true;
        }
      }

      startTimer() {
        if (this.timer == 0 && this.state.seconds > 0) {
          this.timer = setInterval(this.countDown, 1000);
        }
      }

      countDown() {
        // Remove one second, set state so a re-render happens.
        let seconds = this.state.seconds - 1;
        this.setState({
          time: this.secondsToTime(seconds),
          seconds: seconds,
        });
        
        // Check if we're at zero.
        if (seconds == 0) { 
          clearInterval(this.timer);
        }
      }


    render() {
        return(
            (this.state.showResult) ? <ShowResult /> :
            (this.state.showLoginPage) ? <LoginPage /> : 
            <div className="d-flex flex-column">
                <div className="col-md-12 d-flex justify-content-center">
                        EXAM CATEGORY: {(this.context.examCategory === "physics") ? "PHYSICS" : ""}
                </div>
                <div className="d-flex flex-column mb-2">
                    <div className="col-md-12 d-flex justify-content-center">
                        {`0${this.state.time.m}`} : {(this.state.time.s >= 0 && this.state.time.s <=9) ? 0 : ""}{this.state.time.s}
                        {(this.state.time.m === 0 && this.state.time.s === 0) ? <button className="btn btn-primary" onClick={(event)=>{this.setState({showResult: true})}}>Exam Over Submit</button> : "" }
                    </div>
                    <div className="col-md-6 d-flex flex-column mb-2">
                        <div className="col-md-12 d-flex justify-content-left mb-2">
                            {`Question ${this.state.index+1} 0f ${this.context.physicsQuestions.length}`}
                        </div>
                         <div className="col-md-12 d-flex justify-content-left mb-2">
                                {this.context.physicsQuestions[this.state.index].question}
                        </div>
                        
                        <div className="col-md-12 d-flex justify-content-left mb-2">
                            <div className="d-flex flex-column mb-2">
                                {(this.context.physicsQuestions[this.state.index].options.map((option,index) => {
                                    return(                                        
                                        <div className="col-md-12 mb-2">
                                            <input checked={this.state.selectedId == option.id} type="radio" id={option.id} name="physicsquestions" value={option.value} onClick={ (event) => { 
                                                this.setState({selectedId: event.target.id}); 
                                                this.context.selectedOptions[this.state.index] = parseInt(event.target.id);
                                                this.context.setSelectedOptions(this.context.selectedOptions);
                                            }}/>
                                            <label for="vehicle1">{option.value}</label>
                                        </div>                                       
                                    )
                                }))}
                            </div>
                        </div>                    
                        <div className="col-md-12 d-flex justify-content-left">
                            <button type="button" class="btn btn-light" 
                                onClick={(event) => {
                                    if(this.state.index > 0) {
                                        this.state.index = this.state.index - 1; 
                                        this.setState({index: this.state.index});
                                    }
                                    else {
                                        this.setState({modalIsOpen: true});
                                    }
                                }}
                            >   {(this.state.index > 0) ? "Back" : "Exit"}
                            </button>
                            <button type="button" class="btn btn-primary" onClick={(event) => {
                                if(this.state.index < this.context.physicsQuestions.length-1) {
                                    this.state.index = this.state.index + 1; 
                                    this.setState({index: this.state.index});
                                }
                                else{
                                    this.setState({modalIsOpen: true});
                                }
                            }}> {(this.state.index === this.context.physicsQuestions.length-1) ? "Submit" : "Next"}
                            </button>
                        </div>
                    </div>
                </div>
                <Modal
                    isOpen={this.state.modalIsOpen}
                    onAfterOpen={this.state.afterOpenModal}
                    onRequestClose={this.state.closeModal}
                    style={customStyles}
                    contentLabel="Example Modal"
                >
                    {/* <h2 ref={(_subtitle) => (subtitle = _subtitle)}>Hello</h2> */}
                    <button onClick={(event)=>{this.setState({closeModal: true, modalIsOpen: false})}}>close</button>
                    <div>{(this.state.index === this.context.physicsQuestions.length-1) ? "Are you sure you want to submit exam" : (this.state.index == 0) ? "Are you sure you want to exit exam" : "" }</div>
                    <button onClick={(event) => { 
                                if(this.state.index === this.context.physicsQuestions.length-1) {
                                    this.setState({showResult: true}); 
                                }
                                if(this.state.index === 0) { 
                                    this.setState({showLoginPage: true});
                                }
                            }}>Continue</button>
                    
                </Modal>  
            </div>
        )
    }
}

