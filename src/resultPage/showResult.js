import React, { Component } from 'react';
import { AppContext } from '../context';
import DonutChart from 'react-donut-chart';
import LoginPage from '../loginPageDetails/loginPage';

export default class ShowResult extends Component {
    static contextType = AppContext;
    constructor(props) {
      super(props);
      this.state = {
          showLoginPage: false,
          correctAnswers: 0,
          wrongAnswers: 0,
          skippedQuestions: 0,
      }
    }

    componentDidMount() {
        if(this.context.examCategory === "sports") {
            for(var i=0; i<this.context.selectedOptions.length; i++) {
                console.log("Correct Options", this.context.sportsQuestions[i].correct_option);
                if(this.context.selectedOptions[i] === this.context.sportsQuestions[i].correct_option) {
                    this.state.correctAnswers = this.state.correctAnswers + 1;
                }
                else {
                    this.state.wrongAnswers = this.state.wrongAnswers + 1;
                }
                if(this.context.selectedOptions[i] == 0) {
                    this.state.skippedQuestions = this.state.skippedQuestions + 1;
                }
            }
        }
        if(this.context.examCategory === "physics") {
            for(var i=0; i<this.context.selectedOptions.length; i++) {
                console.log("Correct Options", this.context.physicsQuestions[i].correct_option);
                if(this.context.selectedOptions[i] === this.context.physicsQuestions[i].correct_option) {
                    this.state.correctAnswers = this.state.correctAnswers + 1;
                }
                else {
                    this.state.wrongAnswers = this.state.wrongAnswers + 1;
                }
                if(this.context.selectedOptions[i] == 0) {
                    this.state.skippedQuestions = this.state.skippedQuestions + 1;
                }
            }
        }
        if(this.context.examCategory === "history") {
            for(var i=0; i<this.context.selectedOptions.length; i++) {
                console.log("Correct Options", this.context.historyQuestions[i].correct_option);
                if(this.context.selectedOptions[i] === this.context.historyQuestions[i].correct_option) {
                    this.state.correctAnswers = this.state.correctAnswers + 1;
                }
                else {
                    this.state.wrongAnswers = this.state.wrongAnswers + 1;
                }
                if(this.context.selectedOptions[i] == 0) {
                    this.state.skippedQuestions = this.state.skippedQuestions + 1;
                }
            }
        }
        if(this.context.examCategory === "arts") {
            for(var i=0; i<this.context.selectedOptions.length; i++) {
                console.log("Correct Options", this.context.artsQuestions[i].correct_option);
                if(this.context.selectedOptions[i] === this.context.artsQuestions[i].correct_option) {
                    this.state.correctAnswers = this.state.correctAnswers + 1;
                }
                else {
                    this.state.wrongAnswers = this.state.wrongAnswers + 1;
                }
                if(this.context.selectedOptions[i] == 0) {
                    this.state.skippedQuestions = this.state.skippedQuestions + 1;
                }
            }
        }
        this.setState({correctAnswers: this.state.correctAnswers, wrongAnswers: this.state.wrongAnswers, skippedQuestions: this.state.skippedQuestions});
    }
        
    render() {
        return(
            (this.state.showLoginPage) ? <LoginPage /> :
            <div className="d-flex flex-column">
                <div className="col-md-12 d-flex justify-content-center mb-5">
                        EXAM CATEGORY: {(this.context.examCategory === "sports") ? "SPORTS" : (this.context.examCategory === "physics") ? "PHYSICS" : (this.context.examCategory === "history") ? "HISTORY" : (this.context.examCategory === "arts") ? "ARTS" : ""}
                </div>
                <DonutChart
                    data={[{
                        label: 'Correct Answers',
                        value: (this.context.examCategory === "sports") ? Math.round((this.state.correctAnswers/this.context.sportsQuestions.length) * 100) :
                               (this.context.examCategory === "physics") ? Math.round((this.state.correctAnswers/this.context.physicsQuestions.length) * 100) :
                               (this.context.examCategory === "history") ? Math.round((this.state.correctAnswers/this.context.historyQuestions.length) * 100) :
                               (this.context.examCategory === "arts") ? Math.round((this.state.correctAnswers/this.context.artsQuestions.length) * 100) : ""
                    },
                    {
                        label: 'Wrrong Answers',
                        value:  (this.context.examCategory === "sports") ? Math.round((this.state.wrongAnswers/this.context.sportsQuestions.length) * 100) :
                                (this.context.examCategory === "physics") ? Math.round((this.state.wrongAnswers/this.context.physicsQuestions.length) * 100) :
                                (this.context.examCategory === "history") ? Math.round((this.state.wrongAnswers/this.context.historyQuestions.length) * 100) :
                                (this.context.examCategory === "arts") ? Math.round((this.state.wrongAnswers/this.context.artsQuestions.length) * 100) : ""
                    },
                    {
                        label: 'Skipped Answers',
                        value:  (this.context.examCategory === "sports") ? Math.round((this.state.skippedQuestions/this.context.sportsQuestions.length) * 100) :
                                (this.context.examCategory === "physics") ? Math.round((this.state.skippedQuestions/this.context.physicsQuestions.length) * 100) :
                                (this.context.examCategory === "history") ? Math.round((this.state.skippedQuestions/this.context.historyQuestions.length) * 100) :
                                (this.context.examCategory === "arts") ? Math.round((this.state.skippedQuestions/this.context.artsQuestions.length) * 100) : ""
                    }
                    ]} />
                <div className="col-md-12 d-flex justify-content-center mb-5">
                    <button type="button" class="btn btn-primary" onClick={(event) => {
                        this.context.selectedOptions = [0, 0, 0];
                        this.context.setSelectedOptions(this.context.selectedOptions);
                        this.setState({showLoginPage: true});
                    }}>Exit</button>
                </div>
            </div>
        )
    }
}


