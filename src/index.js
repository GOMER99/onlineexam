import React from 'react';
import ReactDOM from 'react-dom';
import AppProvider from './provider'
import { ToastContainer } from 'react-toastify';
import './index.css';
import LoginPage from './loginPageDetails/loginPage';
import reportWebVitals from './reportWebVitals';

ReactDOM.render(
  
  <React.StrictMode>
    <ToastContainer
      position='top-right'
      autoClose={4000}
      hideProgressBar
      newestOnTop
      closeOnClick
      rtl={false}
      pauseOnVisibilityChange
      draggable
      pauseOnHover
    />  
    <AppProvider>
      <LoginPage />
    </AppProvider>
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
