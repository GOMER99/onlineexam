import React, { Component } from 'react';
import { AppContext } from '../context';
import { ToastContainer, toast } from 'react-toastify';
import SportsQuestions from '../questions/sportsQuestions';
import ArtsQuestions from '../questions/artsQuestions';
import HistoryQuestions from '../questions/historyQuestions';
import PhysicsQuestions from '../questions/physicsQuestions';
import '../App.css';
import './loginPage.css';
import '../bootstrap.min.css';
import '../ReactToastify.min.css';
import loginPageImage from './loginPageImage.png';
import loginPage from './loginPage.jpeg';


export default class LoginPage extends Component {
  static contextType = AppContext;
  constructor(props) {
    super(props);
    this.state = {
      displaySportsQuestions: false,
      displayArtsQuestions: false,
      displayHistoryQuestions: false,
      password: "",
      examCategory: "sports"
    }
  }

  displayQuestions() {
    if(this.context.email === "") {
      // toast.info("Kindly fill the email field")
      window.alert("Kindly fill the email field");
    }
    if(this.context.password === "") {
      window.alert("Kindly fill the password");
    }
    if(this.context.examCategory === "sports") {
      this.setState({displaySportsQuestions: true, displayArtsQuestions: false, displayHistoryQuestions: false, displayPhysicsQuestions: false});
    }
    else if(this.context.examCategory === "arts") {
      this.setState({displayArtsQuestions: true, displayHistoryQuestions: false, displayPhysicsQuestions: false, displaySportsQuestions: false});
    }
    else if(this.context.examCategory ===  "history") {
      this.setState({displayHistoryQuestions: true, displayPhysicsQuestions: false, displaySportsQuestions: false, displayArtsQuestions: false});
    }
    else if(this.context.examCategory === "physics") {
      this.setState({displayPhysicsQuestions: true,  displaySportsQuestions: false, displayArtsQuestions: false, displayHistoryQuestions: false})
    }
    else if(this.context.examCategory === "select") {
        window.alert("Kindly Choose any of the Exam Category");
      // toast.info("Kindly Choose any of the Exam Category")
    }
    
  }
  render() {
    return(
      (this.state.displaySportsQuestions) ? <SportsQuestions /> :
      (this.state.displayArtsQuestions) ? <ArtsQuestions /> :
      (this.state.displayHistoryQuestions) ? <HistoryQuestions /> :
      (this.state.displayPhysicsQuestions) ? <PhysicsQuestions /> : 
        <div className="row">
          <div className="col-md-6">
            <img src={loginPage} alt="placeholder 960" className="img-responsive" />
          </div>
          <div className="col-md-6" style={{background: "#FFFFFF"}}>
            <div class="h-100 row justify-content-center align-items-center">
              <div className="d-flex flex-column">
                  <div className="col-md-12 mb-2">
                  <h5>User Sign</h5>
                  </div>
                  <div className="col-md-12 d-flex flex-column mb-2">
                    <label className="" for="email">Your Email</label>
                    <input  className="" 
                            type="text" 
                            id="email" 
                            name="email" 
                            placeholder="Enter your Email"
                            value = {this.context.email}
                            onChange={(event) => {this.context.setEmail(event.target.value)}}
                    />
                  </div>
                  <div className="col-md-12 d-flex flex-column mb-2">
                    <label className="" for="password">Password</label>
                    <input  className="" 
                            type="text" 
                            id="password" 
                            name="password" 
                            placeholder="Enter your password"
                            value = {this.context.password}
                            onChange={(event) => {this.context.setPassword(event.target.value)}}
                    />
                  </div>
                  <div className="col-md-12 d-flex flex-column mb-4">
                    <label className="" for="examcategory">Exam Category</label>
                    <select className="" 
                            id="examcategory" 
                            name="examcategory"
                            value={this.context.examCategory}
                            onChange={(event) => {this.context.setExamCategory(event.target.value)}}
                    >
                      <option value="select">Select Your Exam Category</option>
                      <option value="sports">Sports</option>
                      <option value="arts">Arts</option>
                      <option value="history">History</option>
                      <option value="physics">Physics</option>
                    </select> 
                  </div>
                  <div className="col-md-12 d-flex flex-column">                    
                    <button className="btn btn-primary" type="button" onClick={(event) => { this.displayQuestions(); }}>SIGN IN</button>
                  </div>            
              </div>
            </div>
          </div>
        </div>      
     )
    }
}